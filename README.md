# Open Camera Ext
Fork of awesome Open Camera Android app: http://opencamera.sourceforge.net/
The fork contains some minor extensions (thus the name) of the original project:
  - Remembers and restores selected camera (front, back) even when app is completely shutdown or phone is restarted
  - Remembers and restores selected zoom level (independently for front and back) even when app is completely shutdown or phone is restarted
  
# New Features
  - TODO

License
----

Open Camera is released under the GPL v3 or later. The source code is available from https://sourceforge.net/projects/opencamera/files/ .

The following files are used in Open Camera Ext (based on Open Camera):
* earth.png from http://commons.wikimedia.org/wiki/File:NASA_Earth_America_2010.jpg , public domain.
* exposure_locked.png, focus_mode_locked.png modified from, https://www.iconfinder.com/icons/128411/antivirus_close_forbid_hide_lock_locked_password_privacy_private_protection_restriction_safe_secure_security_icon#size=64 , by Aha-Soft, under CC BY 3.0 (no need to credit me).
* exposure_unlocked.png modified from https://www.iconfinder.com/icons/128416/free_freedom_hack_lock_open_padlock_password_secure_security_unlock_unlocked_icon#size=64 , by Aha-Soft, under CC BY 3.0 (no need to credit me).
* flash_red_eye.png, popup_flash_red_eye.png from https://www.iconfinder.com/icons/103177/eye_see_view_watch_icon#size=128 , by Designmodo, under CC BY 3.0 (no need to credit me).
* flash_torch.png, popup_torch.png from https://www.iconfinder.com/icons/51924/bulb_light_icon#size=128 , by IconFinder - http://www.iconfinder.net , by CC BY 3.0.
* focus_mode_macro.png from https://www.iconfinder.com/icons/81105/macro_mb_icon#size=128 , by Yankoa - http://yankoa.deviantart.com/ , under CC BY 3.0.
* gallery.png from https://www.iconfinder.com/icons/6915/book_gallery_images_photos_pictures_icon#size=128 , by Alessandro Rei - http://www.kde-look.org/usermanager/search.php?username=mentalrey , under GPL v3.
* switch_video.png - merged from images https://www.iconfinder.com/icons/81087/mb_photo_icon#size=128 and https://www.iconfinder.com/icons/81197/mb_rec_video_icon#size=128 by Yankoa - http://yankoa.deviantart.com/ , under CC BY 3.0 (no need to credit me).
* take_video.png, take_video_pressed.png from https://www.iconfinder.com/icons/81197/mb_rec_video_icon#size=128 , by Yankoa - http://yankoa.deviantart.com/ , under CC BY 3.0.
* flash_auto.png, flash_off.png, flash_on.png, ic_burst_mode_white_48dp.png, ic_exposure_white_48dp.png, ic_face_white_48dp.png, ic_info_outline_white_48dp.png, ic_help_outline_white_48dp.png, ic_mic_red_48dp.png, ic_mic_white_48dp.png, ic_more_horiz_white_48dp.png, ic_pause_circle_outline_white_48dp.png, ic_photo_camera_white_48dp.png, ic_photo_size_select_large_white_48dp.png, ic_power_settings_new_white_48dp.png, ic_save_white_48dp.png, ic_timer_white_48dp.png, popup*.png, ic_touch_app_white_48dp.png, ic_videocam_white_48dp.png, settings.png, share.png, switch_camera.png, trash.png - from https://developer.android.com/design/downloads/index.html / https://github.com/google/material-design-icons/ / https://design.google.com/icons/, by Google, under CC BY 4.0.
* beep.ogg, beep_hi.ogg - from http://opengameart.org/content/interface-beeps , by bart, under CC0 (public domain).

**Live long and prosper**

3rd Party Libraries
----
* PhotoView https://github.com/chrisbanes/PhotoView (Apache License 2.0)

Copyright 2017 Chris Banes

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
