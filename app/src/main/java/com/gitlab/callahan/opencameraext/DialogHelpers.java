package com.gitlab.callahan.opencameraext;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import net.sourceforge.opencamera.R;

public final class DialogHelpers {
    public static void showErrorDialog (
            Context context, String title, String message) {

        showModalDialog(context, title, message, false);
    }

    public static boolean showOkCancelDialog (
            Context context, String title, String message) {

        return showModalDialog(context, title, message, true);
    }

    private static boolean showModalDialog (Context context, String title, String message, boolean withCancelButton) {
        // make a handler that throws a runtime exception when a message is received
        final Handler handler = new Handler() {
            @Override
            public void handleMessage (Message mesg) {
                throw new RuntimeException();
            }
        };

        final boolean[] _yesClicked = {false};

        // make a text input dialog and show it
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(false);
        alert.setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
            public void onClick (DialogInterface dialog, int whichButton) {
                _yesClicked[0] = true;
                handler.sendMessage(handler.obtainMessage());
            }
        });

        if (withCancelButton) {
            alert.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                public void onClick (DialogInterface dialog, int whichButton) {
                    _yesClicked[0] = false;
                    handler.sendMessage(handler.obtainMessage());
                }
            });
        }

        alert.show();

        // loop till a runtime exception is triggered.
        try {
            Looper.loop();
        } catch (RuntimeException ex) {
        }

        return _yesClicked[0];
    }
}
