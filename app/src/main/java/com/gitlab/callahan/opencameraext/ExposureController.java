package com.gitlab.callahan.opencameraext;

public interface ExposureController {
    void lockAutoExposure ();
    void unlockAutoExposure ();
}
