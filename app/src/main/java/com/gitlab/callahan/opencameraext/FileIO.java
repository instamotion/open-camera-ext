package com.gitlab.callahan.opencameraext;

import java.io.File;

public final class FileIO {
    public static boolean removeDirectoryRecursive (File directory) {
        if (directory.isDirectory()) {
            String[] children = directory.list();
            for (int idx = 0; idx < children.length; idx++) {
                boolean success = removeDirectoryRecursive(new File(directory, children[idx]));
                if (!success) {
                    return false;
                }
            }
        }

        return directory.delete();
    }
}
