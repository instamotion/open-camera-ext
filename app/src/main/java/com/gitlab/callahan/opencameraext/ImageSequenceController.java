package com.gitlab.callahan.opencameraext;

public interface ImageSequenceController {
    void lockAutoExposure ();
    void unlockAutoExposure ();

    boolean isExposureTestImage ();

    ImageSequenceMode getMode ();

    String getImageCommentTemplate ();
    String getOverlayBasePath ();

    String getOutputBasePath ();
    String getOutputFileNamePattern ();
    boolean isSettingImageEditingFlagInFirstImageEnabled ();

    String getOutputImageFilePath ();
    boolean outputFileAlreadyExists ();

    void advanceToNextImage ();
    boolean isCompleted ();
    String getCurrentImageComment ();

    void cleanup ();
}
