package com.gitlab.callahan.opencameraext;

//NOTE: make sure this is always exactly the same as on the IM side...
public enum ImageSequenceMode
{
    // process templates in folder sequentially
    // continue endlessly after last template
    // NO AE lock (exposure reference key shot)
    SEQUENTIAL_OPEN_ENDED,

    // process templates in folder sequentially
    // continue endlessly after last template
    // WITH AE lock (exposure reference key shot)
    SEQUENTIAL_OPEN_ENDED_WITH_EXPOSURE_REFERENCE,

    // process templates in folder sequentially
    // finish with last template
    // WITH AE lock (exposure reference key shot)
    SEQUENTIAL_FULL_SPIN_WITH_EXPOSURE_REFERENCE,

    // process templates in folder in special order and mirror certain images
    // finish with last template
    // WITH AE lock (exposure reference key shot)
    MIRRORED_180_WITH_EXPOSURE_REFERENCE
}
