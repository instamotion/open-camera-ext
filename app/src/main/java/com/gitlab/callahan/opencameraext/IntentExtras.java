package com.gitlab.callahan.opencameraext;

public final class IntentExtras {
    public static final String MODE_ON                      = "on";
    public static final String MODE_OFF                     = "off";
    public static final String MODE_AUTO                    = "auto";

    public static final String PRESET_OUTPUT_DIRECTORY      = "PRESET_OUTPUT_DIRECTORY";
    public static final String PRESET_OUTPUT_FILENAME       = "PRESET_OUTPUT_FILENAME";

    public static final String PRESET_STARTUP_MODE          = "PRESET_STARTUP_MODE";
    public static final String PRESET_STARTUP_PHOTO_MODE    = "photo";
    public static final String PRESET_STARTUP_VIDEO_MODE    = "video";

    public static final String PRESET_HIDE_PREFERENCES          = "PRESET_HIDE_PREFERENCES";

    public static final String PRESET_STARTUP_CAMERA        = "PRESET_STARTUP_CAMERA";
    public static final String PRESET_STARTUP_FRONT_CAMERA  = "front";
    public static final String PRESET_STARTUP_BACK_CAMERA   = "back";

    public static final String PRESET_ZOOM_FACTOR           = "PRESET_ZOOM_FACTOR";

    public static final String PRESET_EXPOSURE_COMPENSATION = "PRESET_EXPOSURE_COMPENSATION";

    // 4032x3024
    public static final String PRESET_CAMERA_RESOLUTION     = "PRESET_CAMERA_RESOLUTION";

    // 2000
    public static final String PRESET_IMAGE_FIXED_Y = "PRESET_IMAGE_FIXED_Y";

    // 4 Mbps
    public static final String PRESET_VIDEO_BITRATE         = "PRESET_VIDEO_BITRATE";

    // 0 => 1920x720
    public static final String PRESET_VIDEO_QUALITY         = "PRESET_VIDEO_QUALITY";

    public static final String PRESET_FLASH                 = "PRESET_CAMERA_FLASH";

    public static final String PRESET_SCENE_MODE            = "PRESET_SCENE_MODE";
    public static final String SCENE_MODE_HDR               = "hdr";

    public static final String PRESET_PHOTO_MODE            = "PRESET_PHOTO_MODE";

    public static final String PRESET_PHOTO_MODE_DRO        = "dro";
    public static final String PRESET_PHOTO_MODE_HDR        = "hdr";
    public static final String PRESET_PHOTO_MODE_STD        = "std";

    public static final String PRESET_ISO_LEVEL             = "PRESET_ISO_LEVEL";

    // 95
    public static final String PRESET_IMAGE_JPG_SAVE_QUALITY = "PRESET_IMAGE_JPG_SAVE_QUALITY";

//    0 = "auto"
//    1 = "incandescent"
//    2 = "fluorescent"
//    3 = "warm-fluorescent"
//    4 = "daylight"
//    5 = "cloudy-daylight"
//    6 = "twilight"
//    7 = "shade"
    public static final String PRESET_WHITE_BALANCE     = "PRESET_WHITE_BALANCE";

    public static final String PRESET_IMAGE_SEQUENCE_OVERLAY_PATH = "PRESET_IMAGE_SEQUENCE_OVERLAY_PATH";

    public static final String PRESET_IMAGE_SEQUENCE_ALLOW_CONTINUATION_AFTER_LAST_OVERLAY =
            "PRESET_IMAGE_SEQUENCE_ALLOW_CONTINUATION_AFTER_LAST_OVERLAY";

    // see ImageSequenceMode enumeration...
    public static final String PRESET_IMAGE_SEQUENCE_MODE =
            "PRESET_IMAGE_SEQUENCE_MODE";

    public static final String PRESET_START_ACQUISITION_DELAY_MS =
            "PRESET_START_ACQUISITION_DELAY_MS";

    public static final String PRESET_SET_IMAGE_EDITING_EXIF_FLAG_FOR_FIRST_IMAGE =
            "PRESET_SET_IMAGE_EDITING_EXIF_FLAG_FOR_FIRST_IMAGE";

    public static final String EXIF_USER_COMMENT            = "EXIF_USER_COMMENT";

    public static final String FOCUS_MODE              = "FOCUS_MODE";

    public static final String PRESET_INSTAMAGIC_BUTTON_CHECKED = "PRESET_INSTAMAGIC_BUTTON_CHECKED";

    // ex.: "16:9=4032x2268;4:3=4032x3024"
    public static final String PRESET_ALLOW_TOGGLE_ASPECT_RATIO = "PRESET_ALLOW_TOGGLE_ASPECT_RATIO";

    public static final String PRESET_ALLOW_MANUAL_EXPOSURE_CORRECTION = "PRESET_ALLOW_MANUAL_EXPOSURE_CORRECTION";
}
