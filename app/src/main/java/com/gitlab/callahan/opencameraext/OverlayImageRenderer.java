package com.gitlab.callahan.opencameraext;

import android.graphics.Bitmap;

public interface OverlayImageRenderer {
    void setOverlayImage (Bitmap overlayImage);
}
