package com.gitlab.callahan.opencameraext;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OverlayImageSequence implements ImageSequenceController {
    private final static int EXPOSURE_TEST_IMAGE_INDEX = -1;
    private final static String MIRRORED_IMAGE_SUFFIX = "_MIRROR_TO_BE_DELETED";
    private static final String TAG = OverlayImageSequence.class.getName ();
    private int _imageIndex;
    private String[] _overlayFilePaths;
    private Bitmap _currentOverlay;
    private OverlayImageRenderer _overlayRenderer;
    private ExposureController _exposureController;
    private String _commentTemplate;
    private String _sequenceCreationTimeStamp;
    private String _overlayBasePath;
    private String _outputBasePath;
    private String _outputFileNamePattern;
    private ImageSequenceMode _sequenceMode;
    private boolean _isSettingImageEditingFlagInFirstImageEnabled;

    public static OverlayImageSequence create (
            ImageSequenceMode mode,
            String pathToOverlays,
            String outputBasePath,
            String outputFileNamePattern,
            String commentTemplate,
            boolean isSettingImageEditingFlagInFirstImageEnabled,
            OverlayImageRenderer overlayRenderer,
            ExposureController exposureController) {

        OverlayImageSequence sequencer = new OverlayImageSequence ();
        sequencer._sequenceMode = mode;
        sequencer._overlayBasePath = pathToOverlays;
        sequencer._outputBasePath = outputBasePath;
        sequencer._outputFileNamePattern = outputFileNamePattern;
        sequencer._commentTemplate = commentTemplate;
        sequencer._overlayRenderer = overlayRenderer;
        sequencer._exposureController = exposureController;
        sequencer._isSettingImageEditingFlagInFirstImageEnabled = isSettingImageEditingFlagInFirstImageEnabled;
        if (mode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED) {
            sequencer._imageIndex = 0;
        } else {
            sequencer._imageIndex = EXPOSURE_TEST_IMAGE_INDEX;
        }

        File baseDirectory = new File (pathToOverlays);
        if (!baseDirectory.exists ()) {
            throw new IllegalArgumentException (
                    "Failed to find overlay base directory: " + pathToOverlays);
        }

        sequencer._overlayBasePath = baseDirectory.getAbsolutePath ();

        File[] overlayFilesFound = baseDirectory.listFiles (new FilenameFilter () {
            @Override
            public boolean accept (File dir, String name) {
                return name.toLowerCase ().endsWith (".png");
            }
        });

        final int numberOfOverlaysFound = overlayFilesFound.length;

        //NOTE: for closed sequences we expect the number of templates to be 12,8 or 4...
        if (mode != ImageSequenceMode.SEQUENTIAL_OPEN_ENDED &&
                mode != ImageSequenceMode.SEQUENTIAL_OPEN_ENDED_WITH_EXPOSURE_REFERENCE) {
            if (numberOfOverlaysFound != 12 &&
                    numberOfOverlaysFound != 8 &&
                    numberOfOverlaysFound != 4) {
                throw new IllegalArgumentException (
                        "Expected to find 12,8 or 4 PNG-template-images templates in path: "
                                + pathToOverlays +
                                ", but found " + numberOfOverlaysFound + " instead!");
            }
        }

        sequencer._overlayFilePaths = new String[overlayFilesFound.length];

        for (int templateIndex = 0; templateIndex < numberOfOverlaysFound; templateIndex++) {
            sequencer._overlayFilePaths[templateIndex] =
                    overlayFilesFound[templateIndex].getPath ();
        }

        sequencer._currentOverlay = sequencer.loadOverlayBitmapByIndex (0);

        if (sequencer._sequenceMode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED) {
            // for this mode directly start off with the first overlay (if present)
            overlayRenderer.setOverlayImage (sequencer._currentOverlay);
        } else {
            // for this mode: no initial overlay, because we do the exposure test shot first...
            overlayRenderer.setOverlayImage (null);
        }

        //FIX: clean up, since we might have already taken a sequence and discarded it
        exposureController.unlockAutoExposure ();

        sequencer._sequenceCreationTimeStamp =
                new SimpleDateFormat ("yyyyMMddHHmmss").format (new Date ());

        return sequencer;
    }

    @Override
    public String getImageCommentTemplate () {
        return _commentTemplate;
    }

    @Override
    public String getOverlayBasePath () {
        return _overlayBasePath;
    }

    @Override
    public String getOutputFileNamePattern () {
        return _outputFileNamePattern;
    }

    @Override
    public String getOutputBasePath () {
        return _outputBasePath;
    }

    @Override
    public String getOutputImageFilePath () {
        //NOTE: images filenames are counted from 001 (only exposure_test has 000)
        String fileName = String.format (_outputFileNamePattern, _imageIndex + 1);
        return new File (_outputBasePath, fileName).toString ();
    }

    @Override
    public boolean outputFileAlreadyExists () {
        File expectedFile = new File (getOutputImageFilePath ());
        return expectedFile.exists ();
    }

    @Override
    public ImageSequenceMode getMode () { return _sequenceMode; }

    @Override
    public void lockAutoExposure () {
        _exposureController.lockAutoExposure ();
    }

    @Override
    public void unlockAutoExposure () {
        _exposureController.unlockAutoExposure ();
    }

    @Override
    public boolean isExposureTestImage () {
        return _imageIndex == EXPOSURE_TEST_IMAGE_INDEX;
    }

    @Override
    public String getCurrentImageComment () {
        final String modifiedComment = getModifiedComment (
                _commentTemplate, _imageIndex);

        return modifiedComment;
    }

    @Override
    public boolean isSettingImageEditingFlagInFirstImageEnabled () {
        return _isSettingImageEditingFlagInFirstImageEnabled;
    }

    private String getModifiedComment (
            String commentTemplate, int imageIndex) {
        // SIL_ANGLE values:
        // 12 images: 1 -> 330, 2 -> 300, 3 -> 270,
        //  8 images: 1 -> 330, 2 -> 270, 3 -> 210, 4 -> 180, 5 -> 150, 6 -> 90, 7 -> 30, 8 -> 0,
        int rotationAngleByImageNumber;
        if (_overlayFilePaths.length == 12) {
            rotationAngleByImageNumber = 330 - imageIndex * 30;
        } else if (_overlayFilePaths.length == 8 && imageIndex >= 0 && imageIndex < 8) {
            final int[] anglesByIndex = new int[]
                    {330, 270, 210, 180, 150, 90, 30, 0};
            rotationAngleByImageNumber = anglesByIndex [imageIndex];
        } else {
            rotationAngleByImageNumber = -1; // no formula yet...
        }

        if (commentTemplate == null) {
            return "";
        }

        final boolean isOpenEnd =
                _sequenceMode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED ||
                _sequenceMode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED_WITH_EXPOSURE_REFERENCE;

        String modifiedComment = isOpenEnd ?
                commentTemplate.replace ("%SIL_ANGLE%", "-1") :
                commentTemplate.replace ("%SIL_ANGLE%", Integer.toString (
                        rotationAngleByImageNumber));

        //FIX: one timestamp for a complete sequence (not for individual images...)
        modifiedComment = modifiedComment.replace (
                "%YYYYMMDDHHMMSS%", _sequenceCreationTimeStamp);

        if (_isSettingImageEditingFlagInFirstImageEnabled) {
            final boolean isFirstImage = _imageIndex == 0;
            final String IMAGE_EDITING_PLACEHOLDER = "%IMAGE_EDITING%";
            if (isFirstImage) {
                modifiedComment = modifiedComment.replace (
                        IMAGE_EDITING_PLACEHOLDER, "1");
            } else {
                modifiedComment = modifiedComment.replace (
                        IMAGE_EDITING_PLACEHOLDER, "0");
            }
        }

        Log.d (
            TAG, "image-idx: " + imageIndex + ", angle: " + rotationAngleByImageNumber + ", comment: " + modifiedComment);

        return modifiedComment;
    }

    @Override
    public void advanceToNextImage () {
        if (isCompleted ()) {
            return;
        }

        _imageIndex++;
        Log.d (TAG, "advance to next bitmap: " + _imageIndex);
        int templateIndex = _imageIndex;
        if (_sequenceMode == ImageSequenceMode.MIRRORED_180_WITH_EXPOSURE_REFERENCE) {
            final boolean firstImageHasBeenAcquired = _imageIndex > 0;
            if (firstImageHasBeenAcquired) {
                if (_imageIndex == _overlayFilePaths.length / 2) {
                    Log.d (TAG, "Image " + _imageIndex + " will not be mirrored!");
                    templateIndex = _overlayFilePaths.length - 1;
                    _imageIndex = templateIndex;
                    Log.d (TAG, "going back to very last image: " + _imageIndex);
                } else if (!isCompleted ()) {
                    String lastImageSavedFileName =
                            String.format (_outputFileNamePattern, _imageIndex);

                    String lastImageSavedFilePath =
                            new File (_outputBasePath, lastImageSavedFileName).toString ();

                    Log.d (TAG, "Last image " + lastImageSavedFilePath + " is mirrored!");

                    Bitmap sourceBitmap = BitmapFactory.decodeFile (lastImageSavedFilePath);

                    Bitmap mirroredBitmap = mirrorBitmapHorizontally (sourceBitmap);
                    final int mirroredIndex = _overlayFilePaths.length - _imageIndex;

                    // construct filename of mirrored image (also store special suffix, so
                    // we can easily clean mirrored images up, since we only need them temporarily
                    // during final animated preview...
                    final String mirroredFileName =
                            String.format (_outputFileNamePattern, mirroredIndex)
                                    .replace (".jpg", MIRRORED_IMAGE_SUFFIX + ".jpg");

                    final String mirroredFilePath = new File (_outputBasePath, mirroredFileName)
                            .toString ();


                    saveBitmap (mirroredBitmap, mirroredFilePath);

                    String userComment = getModifiedComment (
                            _commentTemplate, mirroredIndex - 1);

                    copyExifFromOldFileAndOverwriteComment (lastImageSavedFilePath,
                            mirroredFilePath, userComment);
                }
            }
        }

        final boolean noMoreTemplatesAvailable = templateIndex >= _overlayFilePaths.length;
        if (isCompleted () || noMoreTemplatesAvailable) {
            _overlayRenderer.setOverlayImage (null);
        } else {
            Log.d (TAG, "activating overlay with index " + templateIndex + " for image: " + _imageIndex);
            _currentOverlay = loadOverlayBitmapByIndex (templateIndex);
            _overlayRenderer.setOverlayImage (_currentOverlay);
        }
    }

    @Override
    public boolean isCompleted () {
        if (_sequenceMode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED ||
                _sequenceMode == ImageSequenceMode.SEQUENTIAL_OPEN_ENDED_WITH_EXPOSURE_REFERENCE) {
            return false;
        }

        return _imageIndex >= _overlayFilePaths.length;
    }

    @Override
    public void cleanup () {
        if (_sequenceMode == ImageSequenceMode.MIRRORED_180_WITH_EXPOSURE_REFERENCE) {
            File baseOutputDirectory = new File (_outputBasePath);
            final File[] imagesFound = baseOutputDirectory.listFiles (new FilenameFilter () {
                @Override
                public boolean accept (File dir, String name) {
                    return name.toUpperCase ().endsWith (MIRRORED_IMAGE_SUFFIX + ".JPG");
                }
            });

            for (File imageToDelete : imagesFound) {
                imageToDelete.delete ();
            }
        }
    }


    private Bitmap loadOverlayBitmapByIndex (int overlayIndex) {
        Log.d (TAG, "loading overlay with index: " + overlayIndex);
        // be memory friendy... release mem right away...
        if (_currentOverlay != null) {
            _currentOverlay.recycle ();
            _currentOverlay = null;
        }

        String fileName = _overlayFilePaths[overlayIndex];
        return BitmapFactory.decodeFile (fileName);
    }

    private Bitmap mirrorBitmapHorizontally (Bitmap source) {
        Log.d (TAG, "mirror bitmap");
        Matrix matrix = new Matrix ();
        matrix.preScale (-1.0f, 1.0f);
        Bitmap flippedBitmap = Bitmap.createBitmap (
                source, 0, 0, source.getWidth (), source.getHeight (), matrix, true);

        return flippedBitmap;
    }

    private static void saveBitmap (Bitmap bitmap, String filePath) {
        Log.d (TAG, "saving bitmap: " + filePath);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream ();
        bitmap.compress (Bitmap.CompressFormat.JPEG, 90, bytes);
        File file = new File (filePath);
        try {
            boolean ok = file.createNewFile ();
            if (!ok) {
                throw new IOException ("Failed to created mirror file!");
            }
            FileOutputStream fo = new FileOutputStream (file);
            fo.write (bytes.toByteArray ());
            fo.close ();
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    private static void copyExifFromOldFileAndOverwriteComment (
            String oldPath,
            String newPath,
            String userComment) {
        final String[] attributes = new String[]
                {
                        ExifInterface.TAG_APERTURE,
                        ExifInterface.TAG_DATETIME,
                        ExifInterface.TAG_EXPOSURE_TIME,
                        ExifInterface.TAG_FLASH,
                        ExifInterface.TAG_MAKE,
                        ExifInterface.TAG_MODEL,
                        ExifInterface.TAG_FOCAL_LENGTH,
                        ExifInterface.TAG_GPS_ALTITUDE,
                        ExifInterface.TAG_GPS_ALTITUDE_REF,
                        ExifInterface.TAG_GPS_DATESTAMP,
                        ExifInterface.TAG_GPS_LATITUDE,
                        ExifInterface.TAG_GPS_LATITUDE_REF,
                        ExifInterface.TAG_GPS_LONGITUDE,
                        ExifInterface.TAG_GPS_LONGITUDE_REF,
                        ExifInterface.TAG_GPS_PROCESSING_METHOD,
                        ExifInterface.TAG_GPS_TIMESTAMP,
                        ExifInterface.TAG_IMAGE_LENGTH,
                        ExifInterface.TAG_IMAGE_WIDTH,
                        ExifInterface.TAG_ISO,
                        ExifInterface.TAG_MAKE,
                        ExifInterface.TAG_MODEL,
                        ExifInterface.TAG_ORIENTATION,
                        ExifInterface.TAG_WHITE_BALANCE
                };
        try {
            ExifInterface oldExif = new ExifInterface (oldPath);
            ExifInterface newExif = new ExifInterface (newPath);
            for (String attribute : attributes) {
                String value = oldExif.getAttribute (attribute);
                if (value != null) {
                    newExif.setAttribute (attribute, value);
                }
            }

            // redundantly set %CAM_NAME% in user comment to: TAG_MODEL
            String cameraModel = newExif.getAttribute (ExifInterface.TAG_MODEL);
            if (cameraModel != null) {
                userComment = userComment.replace ("%CAM_NAME%", cameraModel);
            }

            newExif.setAttribute (ExifInterface.TAG_USER_COMMENT, userComment);
            newExif.saveAttributes ();
        } catch (IOException e) {
            e.printStackTrace ();
        }
    }
}
