package net.sourceforge.opencamera;

public enum PhotoMode {
	Standard,
	DRO, // single image "fake" HDR
	HDR, // HDR created from multiple (expo bracketing) images
	ExpoBracketing // take multiple expo bracketed images, without combining to a single image
}

