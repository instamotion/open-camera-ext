#!/usr/bin/env bash

function buildPreparation {
    curl --user "${BB_AUTH_STRING}" \
        -o signing_key.keystore \
        https://api.bitbucket.org/2.0/repositories/instamotion/${PRIVATE_REPO_NAME}/src/${KEY_REVISION}/keys/${KEY_NAME} && \
    mv signing_key.keystore app/signing/
}

function cleanup {
    [ -f app/signing/signing_key.keystore ] && rm -f app/signing/signing_key.keystore
}

function uploadArtifacts {
    for filename in $(ls app/build/outputs/apk/*${BITBUCKET_COMMIT:0:7}*.apk); do
        echo "Uploading: " $(basename ${filename})
        curl -X POST \
            --user "${BB_AUTH_STRING}" \
            "https://api.bitbucket.org/2.0/repositories/instamotion/${PRIVATE_REPO_NAME}/downloads" \
            --form files=@"${filename}"
    done
}
buildPreparation && \
    ./gradlew assembleRelease && \
    uploadArtifacts && \
    cleanup



