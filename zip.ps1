$ZIP_FILE_NAME = $(get-date -f yyyy-MM-dd_HH-mm-ss) + "_OpenCameraExtended.zip";
git archive --format zip --output $ZIP_FILE_NAME master
